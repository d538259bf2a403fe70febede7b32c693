AMI test cert
```
Certificate:
    Serial Number:
      55:fb:ef:87:81:23:00:84:47:17:0b:b3:cd:87:3a:f4
    Signature Algorithm: sha256WithRSAEncryption
    Issuer: CN = DO NOT TRUST - AMI Test PK
    Subject Public Key Info:
      Public Key Algorithm: rsaEncryption
        RSA Public-Key: (2048 bit)
        Modulus:
          00:e7:36:7b:20:92:ba:7f:aa:a3:f6:0e:49:08:87:
          f5:1c:11:33:ba:5d:f8:9b:5c:ed:c7:90:e4:f3:41:
                           .....
```

Private key is inside `FW_priKey.pfx` that was encrypted with the password `abcd`

```
-----BEGIN PRIVATE KEY-----
MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDnNnsgkrp/qqP2
DkkIh/UcETO6XfibXO3HkOTzQQIGQfkXHlKqmRq0ilpW7lvvd1kHEG6Rb/eRYU36
MPVnSfWArXVUDaTcaK3hY4ofWSOwnvkZ9qDofTvB2bEf1pWWEroI/SB1vn2qhC6j
0RNLxrrgzrjN4fAnIUZWQTVh94Hl25z3ROtpehmlahnZdX8/ysaOfrcFrAHhb0bP
lPjh32aNH5hBQM8HwG5TbmPvqtFB7buHkB9ghKbQpqSulVGZiGQhSWLI6fyJKBL0
cws9GWxS/DT98vy0T8pzSRpu+Tfyx1Ft/711h0b7bbX4DtA4N+eRIXLv4jMEPqHG
f5YN62NZAgMBAAECggEBAN6YDtX/LNl3+L4MZrd13L4WuYw0qAE66Bh3IFQoQ8Cu
hxaAtOlU2ObzQ6HpfE0Aw48PfMM73kXyPdYeiEPWaipl1E0FjbA8fxo8cF9ZvAhw
JDWgwKzS1FL+N44FUqHWiY/Z0gRJTkZ5V0WTJVaQjp2bp0Nev4uMd2DXCoO0ezRD
8yFFIhF9lcLNN5LsGjcpqu+i6WHoBdjSjDBJe6eN/77Qw5U5B0mUF4wc467wV+h2
LkR2ncs/vNnzhM4QUZzEhwin/wAK7k6il2rHv6cUbNIabkuCLIHyLaQwaDVq15P0
fSS6bFQs2VmRMciFrrA6stBFtI1OX8DqpH/rpv8tTvkCgYEA+ssjfE4vbHXouJFg
SY71JKhHRYuTQoeWSxVYunMWSVze8Oza6ONQ856qWvMZMXZwfsHHwrpy8j+DGhpm
YGyj2kyrRtGWoYK5nO8iqJsQtQZbbxoIW4D3pYmIgdMjrROxLEZ0f0LBSrtBqFDl
Mrc15Qel5PM7kvkgOruo8KJeAKcCgYEA7ANHfMLsD2HSX4D8VUCjgaU9yweGb+9z
rE/ZwxDyUv6zeCqDwnmYcl8N/rKX8TdeNhVURw45uj2SAY2AEVzXzIFzwlQ7rMjF
YRZAzY8u0NWu5j6bQg2be60FONZxkj6fZ2ACTNXL5Rjk1Oo3y45kSwX2dY6Z7eBH
WkBCETLT+/8CgYEAwFYMR+faXO7sZKgxVRiLDurzhfVuIWbEr56WZp+epvLPk6vC
kzrKOZwkgA3R82onCj9pCQFe6EQnMlyuySJoYyHLdUuFnyYXXbDUPsR1gq92bfVk
FXEJ++1HQHGB+dmRVihObc7vWHXV+9TWgheaAKTl6HlV5fijbAfBn5KWTx8CgYA6
MwDQ6jxN+sBm3qba2wq28KmRRHTJa5HpvvMjIC2xe6+EBW/iaBvIsFoCIcZILHAm
BBJ5Ry/DFmXHzj3ziiKJAq8qD517mTLbIyKtzg4sX1U2399nZXWfBOaRwofVGW4z
jsBidCJIh1/+hRdnMeDXg0voZ1bcCv9taQUhupf9vwKBgQD4RPHwGEOv1SmDKplS
dUYlnrEYzLjJrwVY6MMRzGqCQc7gJQ1XQD7N1eNBIHkiJMyvvJY8eVnanDbFfMJa
SJoRTZCitfG94ltAidOcq4sQ1mX+IPQkGxpx6SMG6VbxUBiCRVlksBJWTpOOI0z6
ktOdp+4ktlsQ5cBMbgp2N6ksYQ==
-----END PRIVATE KEY-----
```

[Archive of entire repo](https://www.mediafire.com/file/ilfkb0g84ql3fal/Ryzen2000_4000-fe2d3445fcac24e7554c4d2d78d236bbaf1a28b8.zip/file)
<br>[archive.org mirror](https://web.archive.org/web/20230514210830if_/https://codeload.github.com/raywu-aaeon/Ryzen2000_4000/zip/fe2d3445fcac24e7554c4d2d78d236bbaf1a28b8)